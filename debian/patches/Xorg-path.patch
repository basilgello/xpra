Last-Update: 2021-02-07
Forwarded: not-needed
Author: Simon Ruderich <simon@ruderich.org>
Reviewed-By: Dmitry Smirnov <onlyjob@member.fsf.org>
Bug-Debian: https://bugs.debian.org/863891
Description: Fix path to Xorg binary in /etc/xpra/conf.d/55_server_x11.conf
 We need the (absolute) path to the non-setuid binary and not to a possibly
 installed setuid-wrapper (which requires root or login on a tty).
 Auto-dection fails when Xorg is not installed in the build environment.
 .
 As the Xorg setuid wrapper is Debian specific (and might be removed in the
 future) there's no need to upstream this change.
 .
 Ideally default "/usr/bin/Xorg" should be replaced with "/usr/lib/xorg/Xorg".
 .
 As of 2021-02-07 Dmitry adjusted this patch to correct only the path to
 Xorg executable in the comment - this way we avoid the mistake of changing
 the upstream method (and preference) of Xorg/xvfb detection that varies
 depending on hardware architecture. Additionally it is much greater (and
 time consuming) effort to maintain a patch comparing to having a few extra
 build-dependencies to ensure consistent detection of xvfb command.

--- a/etc/xpra/conf.d/55_server_x11.conf.in
+++ b/etc/xpra/conf.d/55_server_x11.conf.in
@@ -37,9 +37,9 @@
 #        +extension GLX +extension Composite \
 #        -auth $XAUTHORITY \
 #        -screen 0 7680x4320x24+32
 # - With Xorg 1.12 or newer and the dummy driver:
-# xvfb = /usr/bin/Xorg -noreset -nolisten tcp \
+# xvfb = /usr/lib/xorg/Xorg -noreset -nolisten tcp \
 #        +extension GLX +extension RANDR +extension RENDER \
 #        -auth $XAUTHORITY \
 #        -logfile %(log_dir)s/Xorg.${DISPLAY}.log \
 #        -configdir ${XDG_RUNTIME_DIR}/.xpra/xorg.conf.d/$PID \
